class EditionParser:
    def __init__(self, edition_json):
        self.edition_json = edition_json
        self.template_names = list()
        self.slice_names = list()

    def collect_article_template_data(self):
        def map_slices_in_section(a_section):
            temp = list()
            if 'slices' in a_section:
                for a_slice in a_section['slices']:
                    temp.append(a_slice)
            return temp

        def map_items_in_slice(a_slice):
            temp = list()
            if 'items' in a_slice:
                for an_item in a_slice['items']:
                    temp.append(an_item)
            return temp

        def map_templates_in_item(an_item):
            if 'article' in an_item:
                return an_item['article']['template']

        edition = self.edition_json['data']['edition']
        sections = edition['sections']

        print(f"Processing edition {edition['id']}")

        for section in sections:
            slices = list(map_slices_in_section(section))

            for slice in slices:
                items = list(map_items_in_slice(slice))

                for item in items:
                    self.template_names.append(map_templates_in_item(item))

        return self.template_names

    def collect_slice_data(self):
        def map_slices_in_react_section(a_section):
            temp = list()
            if 'slices' in a_section:
                for a_slice in a_section['slices']:
                    temp.append(a_slice)
            return temp

        def map_name_in_slice(a_slice):
            if 'name' in a_slice:
                return a_slice['name']

        edition = self.edition_json['data']['edition']
        react_sections = edition['reactSections']

        print(f"Processing edition {edition['id']}")

        for react_section in react_sections:
            slices = list(map_slices_in_react_section(react_section))

            for item in slices:
                self.slice_names.append(map_name_in_slice(item))

        return self.slice_names
