**Requires Python 3.10**

```
usage: Main.py [-h] [--count COUNT] [--environment {prod,staging,uat}]
               [--action {templates,slices}] [--tablet TABLET]]
```

```
options:
  -h, --help            show this help message and exit
  --count COUNT, -c COUNT
                        Number of editions to fetch, defaults to 1
  --environment {prod,staging,uat}, -e {prod,staging,uat}
                        TPA environment, defaults to staging
  --action {templates,slices}, -a {templates,slices}
                        Count slices or article templates
  --tablet TABLET, -t TABLET
                        Fetch tablet editions
```