import asyncio


class EditionFetcher:
    def __init__(self, networking, edition_count, is_tablet):
        self.networking = networking
        self.is_tablet = is_tablet
        self.edition_count = edition_count

    async def get_editions_from(self, edition_list_json):
        editions = edition_list_json['data']['editions']['list']
        edition_ids = list(map(lambda item: item['id'], editions))

        print(f"Fetching {self.edition_count} editions from {self.networking.api_url}, isTablet={self.is_tablet}\n")

        return edition_ids

    async def fetch_edition_list(self):
        fetch_edition_list_task = asyncio.create_task(self.networking.fetch_edition_list(self.edition_count))
        await fetch_edition_list_task

        get_edition_list_task = asyncio.create_task(self.get_editions_from(fetch_edition_list_task.result()))
        await get_edition_list_task
        return get_edition_list_task.result()

    async def fetch_edition(self, edition_id, is_tablet):
        fetch_edition_task = asyncio.create_task(self.networking.fetch_edition(edition_id, is_tablet))
        await fetch_edition_task
        return fetch_edition_task.result()
