import sys
import requests
import json
import pandas as pd
import asyncio


class Networking:
    def __init__(self, api_url):
        self.api_url = api_url

    async def fetch_graphql(self, query, variables):
        query_json = {'query': query}
        if variables:
            query_json['variables'] = variables

        request = requests.post(self.api_url, json=query_json)

        if (request.status_code >= 200) and (request.status_code < 400):
            result_json = json.loads(request.text)
            return result_json
        else:
            sys.exit(f"Failed to load request with status {request.status_code}")

    async def fetch_edition_list(self, count):
        with open('./resources/EditionListQuery.graphql', 'r') as editionListQueryFile:
            edition_list_query = editionListQueryFile.read()

        variables = {
            "count": count
        }

        fetch_edition_list_task = asyncio.create_task(self.fetch_graphql(edition_list_query, variables))
        await fetch_edition_list_task
        return fetch_edition_list_task.result()

    async def fetch_edition(self, edition_id, is_tablet):
        with open('./resources/EditionQuery.graphql', 'r') as editionQueryFile:
            edition_query = editionQueryFile.read()

        variables = {
            "id": edition_id,
            "revision": 1,
            "isTablet": is_tablet
        }

        fetch_edition_task = asyncio.create_task(self.fetch_graphql(edition_query, variables))
        await fetch_edition_task
        return fetch_edition_task.result()
