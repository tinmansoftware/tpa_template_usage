import argparse
from collections import Counter
from Networking import *
from EditionFetcher import *
from EditionParser import *


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


# parse command line args
parser = argparse.ArgumentParser()
parser.add_argument('--count', '-c', type=int, help='Number of editions to fetch, defaults to 1', default=1)
parser.add_argument('--environment', '-e', choices=['prod', 'staging', 'uat'], type=str, help='TPA environment, defaults to staging', default='staging')
parser.add_argument('--action', '-a', choices=['templates', 'slices'], type=str, help='Count slices or article templates', default='slices')
parser.add_argument('--tablet', '-t', help='Fetch tablet editions', type=str2bool, nargs='?', const=True, default=False)
args = parser.parse_args()

# Globals
api_urls = {
    'prod': 'https://api.thetimes.co.uk/graphql',
    'staging': 'https://api.staging-thetimes.co.uk/graphql',
    'uat': 'https://api.uat-thetimes.co.uk/graphql'
}
api_url = api_urls[args.environment.lower()]
edition_count = int(args.count)
action_type = args.action.lower()
is_tablet = args.tablet


class TemplateUsage:
    def __init__(self):
        self.expected_editions = list()
        self.completed_editions = list()
        self.template_names = list()
        self.slice_names = list()

    def formatted_article_template_output(self):
        counted = Counter(self.template_names)
        output = ['\n------------------------------',
                  f"\nFound the following article templates in {len(self.completed_editions)} editions:\n"]

        for key, value in sorted(counted.items(), key=lambda item: item[1], reverse=True):
            output.append(f"{key}: {value}")

        output.append('\n------------------------------')
        return '\n'.join(output)

    def formatted_slice_name_output(self):
        counted = Counter(self.slice_names)
        output = ['\n------------------------------',
                  f"\nFound the following slice names in {len(self.completed_editions)} editions:\n"]

        # make a store using all the available slice names
        with open('./resources/SliceNameReference.json', 'r') as all_slice_names_json:
            all_slice_names = json.load(all_slice_names_json)

        # map counted items to existing slice name reference
        for key, value in counted.items():
            all_slice_names[key] = value

        for key, value in sorted(all_slice_names.items(), key=lambda item: item[0], reverse=False):
            output.append(f"{key}: {value}")

        output.append('\n------------------------------')
        return '\n'.join(output)

    async def main(self):
        networking = Networking(api_url)
        edition_fetcher = EditionFetcher(networking, edition_count, is_tablet)

        fetch_editions_task = asyncio.create_task(edition_fetcher.fetch_edition_list())
        await fetch_editions_task
        edition_ids = fetch_editions_task.result()
        self.expected_editions = edition_ids

        for edition_id in edition_ids:
            fetch_edition_task = asyncio.create_task(edition_fetcher.fetch_edition(edition_id, is_tablet))
            await fetch_edition_task
            edition_json = fetch_edition_task.result()
            edition_parser = EditionParser(edition_json)

            match action_type:
                case 'articles':
                    self.template_names.extend(edition_parser.collect_article_template_data())
                    self.completed_editions.append(edition_id)
                    if len(self.expected_editions) == len(self.completed_editions):
                        return self.formatted_article_template_output()
                case 'slices':
                    self.slice_names.extend(edition_parser.collect_slice_data())
                    self.completed_editions.append(edition_id)
                    if len(self.expected_editions) == len(self.completed_editions):
                        return self.formatted_slice_name_output()
                case _:
                    sys.exit('Unknown action')


async def start():
    template_usage_task = asyncio.create_task(TemplateUsage().main())
    await template_usage_task
    print(template_usage_task.result())

asyncio.run(start())
